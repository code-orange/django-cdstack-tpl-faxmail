<?php
require_once('PHPMailer/PHPMailerAutoload.php');

$ini_config = parse_ini_file("/etc/co-faxserver/settings.ini", TRUE);
$main_settings = $ini_config['settings'];

$link = mysqli_connect($main_settings['MAIN_DATABASE_HOST'], $main_settings['MAIN_DATABASE_USER'], $main_settings['MAIN_DATABASE_PASSWD'], $main_settings['MAIN_DATABASE_NAME']);
if (!$link) {
    die('Verbindung schlug fehl: ' . mysqli_error());
}

if($argv[1] == 'failed')
{
	$success_text = "konnte nicht";
}else{
	$success_text = "konnte";
}

$bodytext  = "Das Fax an ".$argv[3]." ".$success_text." zugestellt werden."."\r\n";
$bodytext .= "X-FAX-DOC-ID: ".$argv[4];

$email = new PHPMailer();

$email->SMTPDebug = 2;

$email->SMTPAuth = true;
$email->Host = $main_settings['FAXSERVER_SMTP_OUT_SERVER'];
$email->Mailer = "smtp";
$email->Port = $main_settings['FAXSERVER_SMTP_OUT_PORT'];
$email->Username = $main_settings['FAXSERVER_SMTP_OUT_USERNAME'];
$email->Password = $main_settings['FAXSERVER_SMTP_OUT_PASSWORD'];

$email->From      = 'noreply@example.com';
$email->FromName  = 'noreply@example.com';
$email->Subject   = '[FAX] '.$argv[3].' '.$argv[1];
$email->Body      = $bodytext;
$email->AddAddress( $argv[2] );

$file_to_attach = '/tmp/faxout/'.$argv[4].'/faxout.tif';

$email->AddAttachment( $file_to_attach , 'fax-'.$argv[4].'.tif' );

if($email->Send())
{
	unlink($file_to_attach);
	echo "Mail sent, file $file_to_attach removed.";
}

mysql_close($link);
