#!/bin/bash
set -e

XDG_RUNTIME_DIR=/tmp/runtime-freeswitch
export XDG_RUNTIME_DIR

mkdir -p $XDG_RUNTIME_DIR
chmod 0700 $XDG_RUNTIME_DIR

UUID=$(uuidgen)
VALUE=$(cat)

MAILSENDER=$1

#echo $1 > /tmp/sender.txt
DESTNR=`echo $2 | awk -F'[@]' '{print $1}'`

mkdir -p /tmp/faxout/$UUID
echo "$VALUE" | /usr/bin/xvfb-run /opt/email2pdf/email2pdf -d /tmp/faxout/$UUID

echo $DESTNR > /tmp/faxout/$UUID/faxout.info.txt
echo $2 >> /tmp/faxout/$UUID/faxout.info.txt

gs -q -r204x196 -sPAPERSIZE=a4 -dPDFFitPage -dFIXEDMEDIA -dNOPAUSE -dBATCH -dSAFER -sDEVICE=tiffg3 -sOutputFile=/tmp/faxout/$UUID/faxout.tif /tmp/faxout/$UUID/*.pdf

/usr/bin/fs_cli \
 --execute="originate {origination_caller_id_name='$MAILSENDER',origination_caller_id_number=00000000,ignore_early_media=true,fax_enable_t38=true,fax_enable_t38_request=true,absolute_codec_string='PCMA,PCMU',originate_continue_on_timeout=true,originate_timeout=30,originate_retries=5,originate_retry_sleep_ms=60000,fax_use_ecm=true,execute_on_fax_success='system /etc/freeswitch/scripts/fax_sent_status_event.sh success $MAILSENDER $DESTNR $UUID',fax_transfer_rate=9600,execute_on_fax_failure='system /etc/freeswitch/scripts/fax_sent_status_event.sh failed $MAILSENDER $DESTNR $UUID'}sofia/gateway/gwout/$DESTNR &txfax(/tmp/faxout/$UUID/faxout.tif)"
