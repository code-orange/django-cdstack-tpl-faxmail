#!/bin/bash
#
# $1 is email alias (The dialed #)
# $2 is filename

set -e

tiff2pdf -o /tmp/FAX-$3.pdf /tmp/FAX-$3.tif
#mutt -n -f /dev/null -F /etc/freeswitch/scripts/muttrc -e "my_hdr From: FAX from $2 <$2@example.com>" -s "Fax from $2 (example.com)" kevin.olbrich@dolphin-it.de -a /tmp/FAX-$3.pdf < /dev/null
php /etc/freeswitch/scripts/emailfax.php $1 $2 $3