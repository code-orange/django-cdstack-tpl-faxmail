<?php
require_once('PHPMailer/PHPMailerAutoload.php');

$ini_config = parse_ini_file("/etc/co-faxserver/settings.ini", TRUE);
$main_settings = $ini_config['settings'];

$link = mysqli_connect($main_settings['MAIN_DATABASE_HOST'], $main_settings['MAIN_DATABASE_USER'], $main_settings['MAIN_DATABASE_PASSWD'], $main_settings['MAIN_DATABASE_NAME']);
if (!$link) {
    die('Verbindung schlug fehl: ' . mysqli_error());
}

$query = 'SELECT fax_id, user_id, dest_email FROM data_sip_ext_fax WHERE fax_switch_ext_id = '.$argv[1].';';
$result = mysqli_query($link, $query);
$fax_ext_details = mysqli_fetch_assoc($result);

$bodytext  = "Das Fax von ".$argv[2]." befindet sich im Anhang."."\r\n";
$bodytext .= "X-FAX-DOC-ID: ".$argv[3];

$email = new PHPMailer();

$email->SMTPDebug = 2;

$email->SMTPAuth = true;
$email->Host = $main_settings['FAXSERVER_SMTP_OUT_SERVER'];
$email->Mailer = "smtp";
$email->Port = $main_settings['FAXSERVER_SMTP_OUT_PORT'];
$email->Username = $main_settings['FAXSERVER_SMTP_OUT_USERNAME'];
$email->Password = $main_settings['FAXSERVER_SMTP_OUT_PASSWORD'];

$email->From      = $main_settings['FAXSERVER_SMTP_OUT_FROM_EMAIL'];
$email->FromName  = $argv[2].' via '.$main_settings['FAXSERVER_SMTP_OUT_FROM_NAME'];
$email->Subject   = '[FAX] '.$argv[2].' [FAXID '.$argv[2].'][DOCID '.$argv[3].']';
$email->Body      = $bodytext;
$email->AddAddress( $fax_ext_details['dest_email'] );

$file_to_attach = '/tmp/FAX-'.$argv[3].'.pdf';

$email->AddAttachment( $file_to_attach , 'fax-'.$argv[2].'-'.$argv[3].'.pdf' );

if($email->Send())
{
	unlink($file_to_attach);
	echo "Mail sent, file $file_to_attach removed.";
}else{
    echo "Mail failed.";
}

mysqli_close($link);
